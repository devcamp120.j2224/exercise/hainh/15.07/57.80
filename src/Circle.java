    public class Circle extends Shape {
        double radius ;

    public Circle(){

    }  
    public Circle(double radius){
        this.radius = radius ;
    }  
    public Circle(double radius, String color, boolean filled){
        // super gọi tới phương thức lớp cha 
        super(color, filled);
        this.radius = radius ;
    }

    public double getRadius() {
        return radius;
    }
    public void setRadius(double radius) {
        this.radius = radius;
    }

    // tính diện tích  radius bình phương nhân cho 3,14 là pi
    public double getArea(){
        return radius * radius * Math.PI ;
    }
    // tính chu vi radius nhân 2  rồi nhân với 3,14
    public double getPerimeter(){
        return radius * 2 * Math.PI ;
    }

    @Override
    public String toString() {
        return "Circle [radius= " + getRadius() +  " , phương thức lớp cha : " + super.toString() + "]";
    }


    
}