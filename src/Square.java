public class Square extends Rectangle{
   
    public Square(){

    }
    // thằng này gọi tới constuctor lớp cha là Rectangle(double wild , double length)
    // hình vuông thì 2 cạnh bằng nhau nên khi khởi tạo truyền vào 1 tham số double và gọi tới contructor lớp cha , rồi truyền vào super(side , side)
    public Square(double side){
       super(side , side);
    }
    // giống constructor trên , side = width , length 
    public Square (double side , String color , boolean filled){
        super(side , side , color , filled);
    }

    public double getSide() {
        return getWidth() ;
    }

    public void setSide(double side) {
        setWidth(side);
        setLength(side);
    }

    public void setWidth(double side){
        setSide(side);
    }

    public void setLength(double side){
        setSide(side);
    }
    
    @Override
    public String toString() {
        return "side= " + getSide() + ", phương thức lớp cha = " + super.toString();
    }
}
